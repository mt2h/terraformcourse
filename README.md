# Install
```bash
wget https://releases.hashicorp.com/terraform/0.15.4/terraform_0.15.4_linux_amd64.zip
unzip terraform_0.15.4_linux_amd64.zip
mv terraform /usr/local/bin/terraform
rm terraform_0.15.4_linux_amd64.zip
terraform --version
```

# AWS
```bash
pip3 install -U awscli
aws --version
aws configure
ls ~/.aws
aws sts get-caller-identity
```

# Commands

```bash
terraform init
terraform plan
terraform validate
terraform apply
terraform apply -auto-approve
terraform destroy
terraform destroy -auto-approve
terraform output
ssh -l ec2-user ec2-18-117-70-212.us-east-2.compute.amazonaws.com -i class_key.pem
ssh -l ec2-user ec2-3-137-215-195.us-east-2.compute.amazonaws.com -i class_key.pem
ssh -l ec2-user 3.137.215.195 -i class_key.pem
#scripts user data: /var/lib/cloud/instance/scripts
#log when running started instance:
tail -f /var/log/cloud-init-output.log
terraform plan -var="ami_id=ami-0ba62214afa52bec7"
terraform apply -auto-approve -var="ami_id=ami-0ba62214afa52bec7"
terraform plan -var-file all_vars.tfvars
terraform apply -auto-approve -var-file all_vars.tfvars
terraform show
```

# SSH
```bash
ssh-keygen -m pem -b 4096 -f class_key.pem
```

# Graphs

```bash
apt-get install -y graphviz
terraform graph | dot -Tsvg > graph.svg
```

![Graph content Terraform](./aws/graph.svg)

# Board Terraform
![Board Terraform](./img/board.jpeg)


# Board Terraform AWS
![Board Terraform AWS](./img/board_aws.jpeg)

# Board Modules in Terraform
![Board Modules](./img/modules.jpeg)

# Board AutoScaling in Terraform
![Board AutoScaling](./img/autoscaling.jpeg)

# Board Project Terraform with AWS
![Board Project](./img/project.jpeg)

