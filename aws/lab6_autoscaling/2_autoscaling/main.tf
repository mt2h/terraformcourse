/*
########## DATA ID AFTER CREATED RESOURCES ##########3
instance_stress_0_id = "i-012a6ac173a288a53"
security_group_1_id = "sg-02179133fc455232d" SSHAccess
security_group_2_id = "sg-0d2434b28550a1239" HTTPAccess
subnet_1_id = "subnet-065855e8150a7984e"
subnet_2_id = "subnet-06c7664603e74de47"
subnet_3_id = "subnet-01ffab907ebdd3b6f"
vpc_id = "vpc-00b1c582040239a55"

AMI ID of instance_stress_0_id = "ami-065922ab5389106de"
*/

resource "aws_launch_configuration" "web" {
    name_prefix = "web-1"
    image_id = "ami-065922ab5389106de" #ImageID created from instance stress0 created before
    instance_type = "t2.micro"
    security_groups = ["sg-0d2434b28550a1239"] #HTTPAccess Security Group created before
    associate_public_ip_address = false

    lifecycle {
      create_before_destroy = true
      }
}

resource "aws_security_group" "elb_http" {
    name = "elb_http"
    description = "Allow HTTP traffic to instances through Elastic Load Balancer"
    vpc_id = "vpc-00b1c582040239a55" #VPC ID created before

    ingress {
      from_port = 80
      to_port = 80
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }

    tags = {
      Name = "Allow HTTP through ELB Security Group"
    }
}

resource "aws_elb" "web_elb" {
    name = "web-elb"
    security_groups = ["${aws_security_group.elb_http.id}"]
    subnets = [
      "subnet-065855e8150a7984e",
      "subnet-06c7664603e74de47",
      "subnet-01ffab907ebdd3b6f"
      ] #Subnets ID created before, Public Subnets
    cross_zone_load_balancing = true

    health_check {
      healthy_threshold = 2
      unhealthy_threshold = 2
      timeout = 3
      interval = 30
      target = "HTTP:80/index.php"
    }

    listener {
      instance_port = 80
      instance_protocol = "http"
      lb_port = 80
      lb_protocol = "http"
    }
}

resource "aws_elb_attachment" "baz" {
    elb = "${aws_elb.web_elb.id}"
    instance = "i-012a6ac173a288a53" #Instance stress0 created before
}

resource "aws_autoscaling_group" "web" {
    name = "${aws_launch_configuration.web.name}-asg"

    min_size = 2
    desired_capacity = 2
    max_size = 10

    health_check_type = "ELB"
    load_balancers = ["${aws_elb.web_elb.id}"]

    launch_configuration = "${aws_launch_configuration.web.name}"
    #availability_zones = ["us-east-2a", "us-east-2b", "us-east-2c"]

    enabled_metrics = [
      "GroupMinSize",
      "GroupMaxSize",
      "GroupDesiredCapacity",
      "GroupInServiceInstances",
      "GroupTotalInstances"
    ]

    metrics_granularity = "1Minute"

    vpc_zone_identifier = ["subnet-065855e8150a7984e"] #for example is public but have to private

    #Required to redeploy whitout an outage
    lifecycle {
      create_before_destroy = true
    }

    tag {
      key = "Name"
      value = "web"
      propagate_at_launch = true
    }
}

####### UP CPU Utilization #######
#when to launch new instance
resource "aws_autoscaling_policy" "web_policy_up" {
    name = "web_policy_up"
    scaling_adjustment = 1 #UP
    adjustment_type = "ChangeInCapacity"
    cooldown = 300
    autoscaling_group_name = "${aws_autoscaling_group.web.name}"
}

resource "aws_cloudwatch_metric_alarm" "web_cpu_alarm_up" {
    alarm_name = "web_cpu_alarm_up"
    comparison_operator = "GreaterThanOrEqualToThreshold"
    evaluation_periods = "2"
    metric_name = "CPUUtilization"
    namespace = "AWS/EC2"
    period = "60" #every 60 secondos
    statistic = "Average"
    threshold = "30" #when CPU Utilization over 30% to lunch alarm

    dimensions = {
      AutoScalingGroupName = "${aws_autoscaling_group.web.name}"
    }

    alarm_description = "This metric monitor EC2instance CPU Utilization"
    alarm_actions = ["${aws_autoscaling_policy.web_policy_up.arn}"]
}

####### DOWN CPU Utilization #######
#when to launch new instance
resource "aws_autoscaling_policy" "web_policy_down" {
  name = "web_policy_down"
  scaling_adjustment = -1 #DOWN
  adjustment_type = "ChangeInCapacity"
  cooldown = 300
  autoscaling_group_name = "${aws_autoscaling_group.web.name}"
}

resource "aws_cloudwatch_metric_alarm" "web_cpu_alarm_down" {
    alarm_name = "web_cpu_alarm_down"
    comparison_operator = "LessThanOrEqualToThreshold"
    evaluation_periods = "2"
    metric_name = "CPUUtilization"
    namespace = "AWS/EC2"
    period = "660" #every 660 secondos
    statistic = "Average"
    threshold = "5" #when CPU Utilization under 5% to lunch alarm

    dimensions = {
      AutoScalingGroupName = "${aws_autoscaling_group.web.name}"
    }

    alarm_description = "This metric monitor EC2instance CPU Utilization"
    alarm_actions = ["${aws_autoscaling_policy.web_policy_down.arn}"]
}
