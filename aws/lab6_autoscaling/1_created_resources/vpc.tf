resource "aws_vpc" "vpc" {
    cidr_block = "192.168.0.0/16"
    instance_tenancy = "default"
    enable_dns_hostnames = true
    tags = {
      Name = "VPC Server"
    }
}

resource "aws_subnet" "subnet1" {
    vpc_id = "${aws_vpc.vpc.id}"
    cidr_block = "192.168.10.0/24"
    map_public_ip_on_launch = true
    availability_zone = "us-east-2a"
    tags = {
      Name = "Subnet 1"
    }
}

resource "aws_subnet" "subnet2" {
    vpc_id = "${aws_vpc.vpc.id}"
    cidr_block = "192.168.20.0/24"
    map_public_ip_on_launch = true
    availability_zone = "us-east-2b"
    tags = {
      Name = "Subnet 2"
    }
}

resource "aws_subnet" "subnet3" {
    vpc_id = "${aws_vpc.vpc.id}"
    cidr_block = "192.168.30.0/24"
    map_public_ip_on_launch = true
    availability_zone = "us-east-2c"
    tags = {
      Name = "Subnet 3"
    }
}

resource "aws_internet_gateway" "gw" {
    vpc_id = "${aws_vpc.vpc.id}"
    tags = {
      Name = "Gateway"
    }
}

resource "aws_route_table" "web-public-rt" {
    vpc_id = "${aws_vpc.vpc.id}"
    route {
      cidr_block = "0.0.0.0/0"
      gateway_id = "${aws_internet_gateway.gw.id}"
    }
    tags = {
      Name = "Public subnet RT"
    }
}

resource "aws_route_table_association" "public-subnet1" {
    subnet_id = "${aws_subnet.subnet1.id}"
    route_table_id = "${aws_route_table.web-public-rt.id}"
}

resource "aws_route_table_association" "public-subnet2" {
    subnet_id = "${aws_subnet.subnet2.id}"
    route_table_id = "${aws_route_table.web-public-rt.id}"
}

resource "aws_route_table_association" "public-subnet3" {
    subnet_id = "${aws_subnet.subnet3.id}"
    route_table_id = "${aws_route_table.web-public-rt.id}"
}
