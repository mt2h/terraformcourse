resource "aws_instance" "stress0" {
    ami = "ami-0ba62214afa52bec7"
    instance_type = "t2.micro"
    associate_public_ip_address = true
    subnet_id = "${aws_subnet.subnet1.id}"
    user_data = "${file("userdata.sh")}"
    vpc_security_group_ids = [
      "${aws_security_group.sg-1.id}",
      "${aws_security_group.sg-2.id}"
      ]
    key_name = "key_pair"
    tags = {
       Name = "stress 0"
    }
}
