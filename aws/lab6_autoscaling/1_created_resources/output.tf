output "vpc_id" {
  value = "${aws_vpc.vpc.id}"
}

output "subnet_1_id" {
  value = "${aws_subnet.subnet1.id}"
}

output "subnet_2_id" {
  value = "${aws_subnet.subnet2.id}"
}

output "subnet_3_id" {
  value = "${aws_subnet.subnet3.id}"
}

output "security_group_1_id" {
  value = "${aws_security_group.sg-1.id}"
}

output "security_group_2_id" {
  value = "${aws_security_group.sg-2.id}"
}

output "instance_stress_0_id" {
  value = "${aws_instance.stress0.id}"
}
