resource "aws_key_pair" "key_pair" {
  key_name = "key_pair"
  public_key = "${file("class_key.pem.pub")}"
}
