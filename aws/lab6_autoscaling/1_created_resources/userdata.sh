#!/bin/bash
sudo -i
yum update -y --security
yum -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
yum -y install httpd php stress unzip wget
systemctl enable httpd
systemctl start httpd
cd /var/www/html
#wget http://rhev.usuarioroot.com/terraform-aws/ec2-stress.zip
#unzip ec2-stress.zip
echo '<!DOCTYPE html PUBLIC ".//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml/DTD/xhtml-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>AWS test Web Server Application</title>
  </head>
  <body>
    <?php
      # Stress the system for a maximum of 10 minutes. Kill all stress processes when requested bythe user
      $stressOrKill = $_GET["stress"];

      if(strlen($stressOrKill) > 0) {

        if($stressOrKill == "start") {
          echo("<h2>Generating Load</h2>");
          exec("stress --cpu 4 --io 1 --vm 1 --vm-bytes 128M--timeout 600s > /dev/null 2>/dev/null &");
          }
        elseif ($stressOrKill == "stop") {
          exec("kill -9 (pidof stress)");
          echo("<h2>Killed stress processes</h2>");
          }

        }
    ?>
    <div id="content">
      <center>
        <h2>Generate Load</h2>
        <table border="0" width="30%" cellpadding="0" cellspacing="0" id="content-table">
          <tr>
            <td>
              <form action="index.php">
                <input type="hidden" name="stress" value="start" />
                <input type="submit" value="Start Stress" />
              </form>
            </td>
            <td>
              <form action="index.php">
                <input type="hidden" name="stress" value="stop" />
                <input type="submit" value="Stop Stress" />
              </form>
            </td>
          </tr>
        </table>
      </center>
    </div>
  </body>
</html>
' > index.php

#echo 'UserData has been successfully executed. ' >> /home/ec2-user/result
#find -wholename /root/.*history -wholename /home/*/*.history -exec rm -f {} \;
#find / -name 'authorized_keys -exec rm -f {} \;'
#rm -rf /var/lib/cloud/data/scripts/*

systemctl restart httpd
