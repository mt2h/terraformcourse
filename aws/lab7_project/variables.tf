variable "region" {
  default = "us-east-2"
}

variable "amis" {
  default = {
    "us-east-2" = "ami-0ba62214afa52bec7"
    }
}

variable "vps_cidr" {
  default = "10.0.0.0/16"
}

variable "public_subnet_cidr" {
  default = "10.0.30.0/24"
}

variable "private_subnet_cidr" {
  default = "10.0.40.0/24"
}

variable "key_name" {}
variable "key_path" {}
