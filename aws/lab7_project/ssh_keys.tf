resource "aws_key_pair" "sample_key_name" {
  key_name = "${var.key_name}"
  public_key = "${file("${var.key_path}")}"
}
