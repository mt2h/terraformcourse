resource "aws_security_group" "nat" {
    name = "vpc_nat"
    vpc_id = "${aws_vpc.default.id}"

    ingress {
      from_port = 80
      to_port = 80
      protocol = "tcp"
      cidr_blocks = ["${var.private_subnet_cidr}"]
    }

    ingress {
      from_port = 443
      to_port = 443
      protocol = "tcp"
      cidr_blocks = ["${var.private_subnet_cidr}"]
    }

    ingress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
      from_port = -1
      to_port = -1
      protocol = "icmp"
      cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
      from_port = 80
      to_port = 80
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
      from_port = 443
      to_port = 443
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = ["${var.vps_cidr}"]
    }

    egress {
      from_port = 0
      to_port = 0
      protocol = "icmp"
      cidr_blocks = ["0.0.0.0/0"]
    }
}

resource "aws_instance" "nat" {
    #https://us-east-2.console.aws.amazon.com/ec2/v2/home?region=us-east-2#LaunchInstanceWizard:
    #AMI NAT FROM Cominity AMIS AWS
    ami = "ami-00d1f8201864cc10c"
    availability_zone = "us-east-2a"
    instance_type = "t2.micro"
    key_name = "${var.key_name}"
    vpc_security_group_ids = ["${aws_security_group.nat.id}"]
    subnet_id = "${aws_subnet.us-east-2a-public.id}"
    associate_public_ip_address = true
    source_dest_check = false #Used Nat VPN
    tags = {
       Name = "VPC NAT"
    }
}

resource "aws_eip" "nat" {
    vpc = true
}

resource "aws_eip_association" "nat" {
    instance_id = "${aws_instance.nat.id}"
    allocation_id = "${aws_eip.nat.id}"
}
