resource "aws_vpc" "main1" {
    cidr_block = "${var.vps_cidr}"
    instance_tenancy = "default"
    enable_dns_hostnames = true
    tags = {
      Name = "Main 1"
      Location = "US"
    }
}

resource "aws_subnet" "subnets" {
    #count = length(var.azs)
    count = length(data.aws_availability_zones.azs.names)
    vpc_id = "${aws_vpc.main1.id}"
    cidr_block = "${element(var.subnet_cidr, count.index)}"
    #availability_zone = "${element(var.azs, count.index)}"
    availability_zone = "${element(data.aws_availability_zones.azs.names, count.index)}" #get one data from list datasources of availability zone according AWS project
    tags = {
      Name = "Subnet ${count.index + 1}"
    }
}

resource "aws_internet_gateway" "gw" {
    vpc_id = "${aws_vpc.main1.id}"
    tags = {
      Name = "VPC Main"
    }
}

resource "aws_route_table" "web-public-rt" {
    vpc_id = "${aws_vpc.main1.id}"
    route {
      cidr_block = "0.0.0.0/0"
      gateway_id = "${aws_internet_gateway.gw.id}"
    }
    tags = {
      Name = "Public subnet RT"
    }
}

resource "aws_route_table_association" "public-subnet" {
    count = length(var.subnet_cidr)
    subnet_id = "${element(aws_subnet.subnets.*.id, count.index)}" # LOOP: the * is for all content of data of resources created in subnets with count replicas
    route_table_id = "${aws_route_table.web-public-rt.id}"
}
