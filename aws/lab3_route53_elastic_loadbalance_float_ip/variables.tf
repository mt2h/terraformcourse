variable "region" {
  default = "us-east-2"
}

variable "vps_cidr" {
  default = "192.168.0.0/16"
}
