resource "aws_vpc" "main1" { #VPC
    cidr_block = "${var.vps_cidr}"
    instance_tenancy = "default"
    enable_dns_hostnames = true
    tags = {
      Name = "Main 1"
      Location = "US"
    }
}

resource "aws_subnet" "subnet1" { #VPC
    vpc_id = "${aws_vpc.main1.id}"
    cidr_block = "192.168.10.0/24"
    availability_zone = "us-east-2a"
    tags = {
      Name = "Subnet 1"
    }
}

resource "aws_subnet" "subnet2" { #VPC
    vpc_id = "${aws_vpc.main1.id}"
    cidr_block = "192.168.20.0/24"
    availability_zone = "us-east-2b"
    tags = {
      Name = "Subnet 2"
    }
}

resource "aws_subnet" "subnet3" { #VPC
    vpc_id = "${aws_vpc.main1.id}"
    cidr_block = "192.168.30.0/24"
    availability_zone = "us-east-2c"
    tags = {
      Name = "Subnet 3"
    }
}

resource "aws_internet_gateway" "gw" { #VPC
    vpc_id = "${aws_vpc.main1.id}"
    tags = {
      Name = "VPC Main"
    }
}

resource "aws_route_table" "web-public-rt" { #VPC
    vpc_id = "${aws_vpc.main1.id}"
    route {
      cidr_block = "0.0.0.0/0"
      gateway_id = "${aws_internet_gateway.gw.id}"
    }
    tags = {
      Name = "Public subnet RT"
    }
}

resource "aws_route_table_association" "public-subnet-1" { #VPC
    subnet_id = "${aws_subnet.subnet1.id}"
    route_table_id = "${aws_route_table.web-public-rt.id}"
}

resource "aws_route_table_association" "public-subnet-2" { #VPC
    subnet_id = "${aws_subnet.subnet2.id}"
    route_table_id = "${aws_route_table.web-public-rt.id}"
}

resource "aws_route_table_association" "public-subnet-3" { #VPC
    subnet_id = "${aws_subnet.subnet3.id}"
    route_table_id = "${aws_route_table.web-public-rt.id}"
}
