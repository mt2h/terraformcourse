resource "aws_security_group" "sg-1" { #EC2
  name = "sg_ping_ssh"
  description = "Allow ping and SSH"
  vpc_id = "${aws_vpc.main1.id}"

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = -1 #from anywhere
    to_port = -1 #from anywhere
    protocol = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0 #all
    to_port = 0 #all
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Ping and SSH"
  }
}

resource "aws_security_group" "sg-2" { #EC2
  name = "sg_http"
  description = "Allow HTTP and HTTPS"
  vpc_id = "${aws_vpc.main1.id}"

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "HTTP and HTTPS"
  }
}
