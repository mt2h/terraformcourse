resource "aws_instance" "server1" { #EC2
    #count = 1
    #count = 3
    count = length(var.subnet1_private_ip)
    ami = "${var.ami_id}"
    instance_type = "t2.micro"
    subnet_id = "${aws_subnet.subnet1.id}"
    associate_public_ip_address = true
    #private_ip = "${var.private_ip}"
    private_ip = "${element(var.subnet1_private_ip, count.index)}" #element return one data of list with index count
    key_name = "class_key1"
    vpc_security_group_ids = [
      "${aws_security_group.sg-1.id}",
      "${aws_security_group.sg-2.id}"
      ]
    user_data = "${file("userdata.sh")}"
    tags = {
      #Name = "Server 1"
      Name = "Server ${count.index + 1}"
      Owner = "mt2h"
      Env = "dev"
    }
}

/*resource "aws_instance" "server2" { #EC2
    count = 1
    ami = "ami-0ba62214afa52bec7"
    instance_type = "t2.micro"
    subnet_id = "${aws_subnet.subnet1.id}"
    associate_public_ip_address = true
    private_ip = "10.0.10.11"
    key_name = "${aws_key_pair.key-class-2.id}"
    vpc_security_group_ids = [
      "${aws_security_group.sg-1.id}",
      "${aws_security_group.sg-2.id}"
      ]
    user_data = <<EOF
#!/bin/bash
export PATH=$PATH:/usr/local/bin
sudo -i
exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1
yum install -y httpd
echo "<html><h1>Helow, this is test</h1></html>" > /var/www/html/index.html
systemctl start httpd
systemctl enable httpd
EOF
    tags = {
      Name = "server2"
      Owner = "mt2h"
      Env = "dev"
    }
}*/
