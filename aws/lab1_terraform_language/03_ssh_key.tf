resource "aws_key_pair" "key-class-1" { #EC2
  key_name = "class_key1"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCWSu3JLsRUVLfvp5TYIvB8WJG1AfuN2py0zRu+GE76eTiIfhJ+UFV/cSaWPceyCRtO4rFKiKNT5DmnMve6K3rPwMX81qErwe1YmI577u7Ih+z2O8z6JF5v+1PEB/FwLgahiOxgV96XQY7UYTBoTxv/qFaoYW5iaAuMGw+z+ZGlIgnioQ86Y0oauCOZBe7xmIICF3HwXc+DuvEz/vpKj1SfDR6EMwQt6VLJerkRFyeeyNjMh4qyEko7aWWg6yln1DFAKYXS222A7qCJ5B/tcWmg0Crf8Mw6QvcxQ5bqq3NZR7tRw5BkJiYnKeMxEmWxNj7llO2VuxFu2aT0rQWXhydjri68+UqLsgyiDAeTZjoUqhH2OVnzAaXfp7eZtZ7tAB/KPrlFJYxlbUF/hlO8TJcSTGyJwdScieNu3tYRaYzqCozYZ/GMpZ7eA+LLAqIj7WYpR/846ktWHvOzrEfbDmuA7sZNaS+F7nre3xWoXnD2GnkrlvjjMoO4s7zYwU8tjIK78yaHtXjC/waUZY/bqV38rEcBVCB1qGFw2vwjQRDYayczXYlErJgC6rIBnyWAqeZB1ykJd3udnnWvvroVMFU5nq6zr7qBDhBVN72rONgl7+Z2lBC2wdflPEGYM3KabKkCOVzTYGKbVmzVylTDuq2EOdmWL5F7o0BlFBalksmVlw=="
}

resource "aws_key_pair" "key-class-2" { #EC2
  key_name = "class_key2"
  public_key = "${file("class_key.pem.pub")}"
}
