terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region = "${var.region}"
}

resource "aws_vpc" "main1" { #VPC
    #cidr_block = "10.0.0.0/16"
  cidr_block = "${var.vpc_cidr}"
  enable_dns_hostnames = true
  tags = {
    Name = "Main VPC 1"
  }
}

resource "aws_subnet" "subnet1" { #VPC
    vpc_id = "${aws_vpc.main1.id}"
    #cidr_block = "10.0.10.0/24"
    cidr_block = "${var.subnet1_cidr}"
    map_public_ip_on_launch = true
    #availability_zone = "us-east-2a"
    availability_zone = "${var.subnet1_zone_2a}"
    tags = {
      Name = "Subnet 1 in ${var.subnet1_zone_2a}"
    }
}

resource "aws_subnet" "subnet2" { #VPC
    vpc_id = "${aws_vpc.main1.id}"
    #cidr_block = "10.0.20.0/24"
    cidr_block = "${var.subnet2_cidr}"
    map_public_ip_on_launch = true
    #availability_zone = "us-east-2b"
    availability_zone = "${var.subnet2_zone_2b}"
    tags = {
      Name = "Subnet 2 in ${var.subnet2_zone_2b}"
    }
}

resource "aws_subnet" "subnet3" { #VPC
    vpc_id = "${aws_vpc.main1.id}"
    #cidr_block = "10.0.30.0/24"
    cidr_block = "${var.subnet3_cidr}"
    map_public_ip_on_launch = true
    #availability_zone = "us-east-2c"
    availability_zone = "${var.subnet3_zone_2c}"
    tags = {
      Name = "Subnet 3 in ${var.subnet3_zone_2c}"
    }
}

resource "aws_internet_gateway" "gw" { #VPC
    vpc_id = "${aws_vpc.main1.id}"
    tags = {
      Name = "Gateway Main 1"
    }
}

resource "aws_route_table" "r" { #VPC
    vpc_id = "${aws_vpc.main1.id}"
    route {
      cidr_block = "0.0.0.0/0"
      gateway_id = "${aws_internet_gateway.gw.id}"
    }
}

resource "aws_route_table_association" "table_subnet1" { #VPC
  subnet_id = "${aws_subnet.subnet1.id}"
  route_table_id = "${aws_route_table.r.id}"
}

resource "aws_route_table_association" "table_subnet2" { #VPC
  subnet_id = "${aws_subnet.subnet2.id}"
  route_table_id = "${aws_route_table.r.id}"
}

resource "aws_route_table_association" "table_subnet3" { #VPC
  subnet_id = "${aws_subnet.subnet3.id}"
  route_table_id = "${aws_route_table.r.id}"
}
