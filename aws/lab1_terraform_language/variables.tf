variable "ami_id" {
  #type = string
  #default = "ami-0ba62214afa52bec7"
  description = "AMI ID for RHRL8"
}

variable "region" {
  #default = "us-east-2"
  description = "Default use form AWS"
}

variable "vpc_cidr" {
  #default = "10.0.0.0/16"
  description = "VPC cidr"
}

variable "subnet1_cidr" {
  #default = "10.0.10.0/24"
  description = "Subnet 1 cidr"
}

variable "subnet2_cidr" {
  #default = "10.0.20.0/24"
  description = "Subnet 2 cidr"
}

variable "subnet3_cidr" {
  #default = "10.0.30.0/24"
  description = "Subnet 3 cidr"
}

variable "subnet1_zone_2a" {
  #default = "us-east-2a"
  description = "Subnet 1 availability zone"
}

variable "subnet2_zone_2b" {
  #default = "us-east-2b"
  description = "Subnet 2 availability zone"
}

variable "subnet3_zone_2c" {
  #default = "us-east-2c"
  description = "Subnet 3 availability zone"
}

/*variable "private_ip" {
  #default = "10.0.10.10"
  description = "Private IP of instance"
}*/

variable "subnet1_private_ip" {
  type = list
  default = ["10.0.10.11", "10.0.10.12", "10.0.10.13"]
  description = "List of IP for subnet 1"
}
