/*resource "aws_iam_group" "administrators" { #IAM
  name = "Administrators"
}

resource "aws_iam_policy_attachment" "admins-attach" { #IAM
  name = "admin-attach"
  groups = ["${aws_iam_group.administrators.name}"]
  policy_arn = "arn:aws:iam::aws:policy/job-function/SystemAdministrator"
}

resource "aws_iam_user" "admin1" { #IAM
  name = "admin1"
}

resource "aws_iam_user" "admin2" { #IAM
  name = "admin2"
}

resource "aws_iam_group_membership" "admin-users" { #IAM
  name = "admins-users"
  users = [
      "${aws_iam_user.admin1.name}",
      "${aws_iam_user.admin2.name}",
  ]
  group = "${aws_iam_group.administrators.name}"
}

resource "aws_iam_access_key" "admin1-access" { #IAM
  user = "${aws_iam_user.admin1.name}"
}

resource "aws_iam_access_key" "admin2-access" { #IAM
  user = "${aws_iam_user.admin2.name}"
}

output "admin_1_access_key" {
  value = "${aws_iam_access_key.admin1-access.id}"
}

output "admin_1_secret_key" {
  value = "${aws_iam_access_key.admin1-access.secret}"
  sensitive = true
}

output "admin_2_access_key" {
  value = "${aws_iam_access_key.admin2-access.id}"
}

output "admin_2_secret_key" {
  value = "${aws_iam_access_key.admin2-access.encrypted_secret}"
  #sensitive = true
}*/
