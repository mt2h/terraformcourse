variable "region" {
  #default = "us-east-2"
  default = "us-west-2"
}

variable "ec2_ami" {
  default = {
    us-east-2 = "ami-0ba62214afa52bec7"
    us-west-2 = "ami-0b28dfc7adc325ef4"
  }
}
