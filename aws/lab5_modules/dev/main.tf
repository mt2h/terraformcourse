module "vpc" {
  source = "../modules/vpc"
  vps_cidr = "192.168.0.0/16" #replaced value variable of module
  tenancy = "default" #replaced value variable of module
  vpc_id = "${module.vpc.vpc_id}"
  subnet_id = "${module.vpc.subnet_id}"
  subnet_cidr = "192.168.1.0/24" #replaced value variable of module
  sg_id = "${module.vpc.sg_id}"
}

module "ec2" {
  source = "../modules/ec2"
  ec2_count = 1 #replaced value variable of module
  ami_id = "ami-0ba62214afa52bec7" #replaced value variable of module, this value is empty in this varible in this module
  instance_type = "t2.micro" #replaced value variable of module
  subnet_id = "${module.vpc.subnet_id}" #assing value from other value variable other module
  sg_id = "${module.vpc.sg_id}" #assing value from other value variable other module
}
