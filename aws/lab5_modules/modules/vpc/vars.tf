variable "vps_cidr" {
  default = "10.0.0.0/16"
}

variable "tenancy" {
  default = "dedicated"
}

variable "vpc_hostnames" {
  default = true
}

variable "vpc_id" {} #value after created VPC

variable "subnet_id" {} #value after created Subnet

variable "sg_id" {} #value after created Secutiry Group

variable "subnet_cidr" {
  default = "10.0.1.0/24"
}
