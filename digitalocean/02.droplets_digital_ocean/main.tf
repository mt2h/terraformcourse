data "digitalocean_ssh_key" "mt2hkey" {
  name = "mt2hkey"
}

resource "digitalocean_droplet" "web" {
  image     = "ubuntu-18-04-x64"
  name      = "web-1"
  region    = "nyc1"
  size      = "s-1vcpu-1gb"
  user_data = file("./userdata.yaml")
  ssh_keys  = [data.digitalocean_ssh_key.mt2hkey.id]
}

resource "digitalocean_domain" "testdomain" {
  name       = "test.mt2h.cl"
  ip_address = digitalocean_droplet.web.ipv4_address
}
