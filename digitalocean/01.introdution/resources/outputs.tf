output "ip" {
    value = digitalocean_droplet.web.ipv4_address
}

output "size" {
    value = digitalocean_droplet.web.size
}
