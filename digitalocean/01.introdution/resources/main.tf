data "digitalocean_ssh_key" "example" {
  name = "mt2hkey"
}

resource "digitalocean_droplet" "web" {
  image    = "ubuntu-18-04-x64"
  name     = "web-1"
  region   = "nyc1"
  size     = "s-1vcpu-1gb"
  ssh_keys = [data.digitalocean_ssh_key.example.id]

  connection {
    user        = "root"
    type        = "ssh"
    host        = digitalocean_droplet.web.ipv4_address
    private_key = file("~/.ssh/mt2h")
    }

  /*provisioner "remote-exec" {
    inline = [
        "apt-get update",
        "apt-get install nginx -y"
        ]
    }*/

  provisioner "file" {
    source = "./data.sh"
    destination = "/tmp/data.sh"
    }

  provisioner "remote-exec" {
    inline = [
        "cd /tmp && bash data.sh"
        ]
    }
}

resource "digitalocean_domain" "default" {
  name       = "web.mt2h.cl"
  ip_address = digitalocean_droplet.web.ipv4_address
}
